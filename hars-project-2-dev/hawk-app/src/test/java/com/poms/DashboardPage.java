package com.poms;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {
	@FindBy(css="div.col-12:nth-child(3) > a:nth-child(5)")
	public WebElement seatmapLink;
	@FindBy(id="logout-link")
	public WebElement logOutLink;
	@FindBy(css="div.card:nth-child(2) > div:nth-child(3) > form:nth-child(4) > button:nth-child(2)")
	public WebElement deleteButton;
	@FindBy(id="book-new-trip-link")
	public WebElement bookTripLink;
	
	public DashboardPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void clickSeatmapLink() {
		this.seatmapLink.click();
	}
	public void clickLogOut(){this.logOutLink.click();}
	
	public void clickBookTripLink() {
		this.bookTripLink.click();
	}
	
	public void clickDeleteButton() {
		this.deleteButton.click();
	}
}
